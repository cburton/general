# Introduction
Some scripts for running common ATLAS stuff. Mostly python-based for things like
creating web pages, running rucio commands, dumping plots from a TDirectory 
structure into a matching Unix directory structore, mounting FUSE servers,
running reconstruction software, analyzing LHE XML files, and analyzing xAOD 
files with AnalysisTop and pyROOT.

The code was only intended for personal use, and so it is admittedly not very 
well-documented, and I cannot gurantee that it will work for anyone/everyone.
Email me at burton@utexas.edu if you need help or have questions.

# Short description of each file
1. `CreateHTML.sh`: Based off of code written by @miochoa to make a web page with 
an array of png/eps plots. Creates an HTML page for a Unix directory structure
of png/eps files.

2. `DumpPlots.py`: Looks inside a ROOT file with a TDirectory structre, finds any 
TH* objects, and stores them in a Unix directory structure that matches the 
directory structure. It's output can be fed into the CreateHTML.sh script.

3. `MountEOS.sh`: mounts EOS user directory in the home directory of a local
workstation.

4. `NtupleToDataframe.py`: Reads in a list of root files, opens them in uproot,
joins them in a Pandas dataframe, and stores the dataframe as an HDF file. Choose
specific brances to store with -b option (comma separation and * wildcarding 
supported). Example usage:  
        `python NtupleToDataframe.py input.txt output.h5 -b myBranch1,myBranch2`

5. `WeightedLHEMerge.py`: used for post-procesing of LHE generation when multiple
diagrams are involved, but merging by cross section is not done automatically.
Also features the ability to pull objects and quanties for plotting.

6. `generatePRW.py`: My own version of the AnalysisTop script for 
pileup-reweighting that actually works for me. (The standard one always crashes 
when I try to use it.)

7. `pyRucio.py`: Performs common rucio operations. Downloads a number of files 
from a list of datasets, checks for existance of DAOD files from a list of AODs,
for both data and Monte Carlo. Examples of usage:  
	    `python pyRucio.py --getMCDAOD --dir /data_ceph/cburton/BPHY9_mc/ --nFiles 2 --datasetFile daod_list.txt`

8. `shReco.sh`: Runs a parallelized Reco_tf.py AOD->DAOD transformation for a 
list of input files. Stores the output DAOD files and the logs.

9. `xAODAnalysis.py`: Counts events or makes plots from a list of input files. 
Additional functionality may be added in the future.
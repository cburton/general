#!/usr/bin/env python
from __future__ import print_function
import ROOT
import sys
from glob import glob
ROOT.xAOD.Init().isSuccess()

def print_auxitem(auxName, auxData):
    auxId = auxData.dataID()
    auxIndex = auxData.index()
    if auxData.isDefaultIndex():
        print('%s: None' % auxName)
    else:
        print('%s: %s[%d]' % (auxName, auxId, auxIndex))

def dump_auxitem(obj, auxName, auxId):
    registry = ROOT.SG.AuxTypeRegistry.instance()
    typename = registry.getTypeName(auxId)
    accessor = ROOT.SG.AuxElement.TypelessConstAccessor(registry.getName(auxId))
    auxData = ROOT.TPython.ObjectProxy_FromVoidPtr(accessor(obj), typename)
    print_auxitem(auxName, auxData)

def dump_auxdata(obj, include=[]):
    registry = ROOT.SG.AuxTypeRegistry.instance()
    auxIds = ROOT.PyDumper.Utils.getAuxIDVector(obj)
    auxIds = dict([(registry.getName(_), _) for _ in auxIds])
    for auxName in include:
        dump_auxitem(obj, auxName, auxIds[auxName])

def get_auxdata(obj, auxName):
    registry = ROOT.SG.AuxTypeRegistry.instance()
    auxIds = ROOT.PyDumper.Utils.getAuxIDVector(obj)
    auxIds = dict([(registry.getName(_), _) for _ in auxIds])
    if auxName in auxIds:
        auxId = auxIds[auxName]
    else:
        print('aux item %s not available in object %s' % (auxName, type(obj)))
    typename = registry.getTypeName(auxId)
    accessor = ROOT.SG.AuxElement.TypelessConstAccessor(registry.getName(auxId))
    return ROOT.TPython.ObjectProxy_FromVoidPtr(accessor(obj), typename)

if len(sys.argv) < 2:
    raise Exception('Usage: TRUTH0_analysis.py INPUTDIRECTORY')
fileDir = sys.argv[1]

fileList = sorted(glob(fileDir + '/*root*'))
print(fileList)

for fileName in fileList:
    DSID = '_'.join(fileName.split('.')[1:3])
    print('Analyzing truth record from', fileName)

    # Get input file
    f = ROOT.TFile.Open(fileName)
    t = ROOT.xAOD.MakeTransientTree(f,'CollectionTree')

    numEntries = t.GetEntries()

    for event in range(numEntries):
        if event % 500 == 0:
            update = 'Analyzing event ( {{:{}d}} / {{}} )'.format(len(str(numEntries-1)))
            print(update.format(event, numEntries))
        t.GetEntry(event)

        for truthMuon in t.TruthMuons:
            dataToDump = ['parentLinks', 'childLinks', 'e_dressed', 'pt_dressed']
            dump_auxdata(truthMuon, dataToDump)

        for onia in t.BPHY9OniaCandidates:
            value = onia.auxdata('float')('Jpsi_mass')
            print(value)
            muonLinks = get_auxdata(onia, 'MuonLinks')
            print(muonLinks)
            # muonLinks = [_.__deref__() for _ in muonLinks]
            # muonLinks = [_.cptr() for _ in muonLinks]
            # print(muonLinks)
        # end event loop

    ROOT.xAOD.ClearTransientTrees()
    f.Close()
    # end file loop

print('Done.')
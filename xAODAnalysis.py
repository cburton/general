#!/usr/bin/env python
from __future__ import print_function
import os,sys,optparse
try:
    import ROOT
except:
    print('Did not setup AthDerivation.')
    sys.exit(1)
from glob import glob

nDebug = 1000

def getDatasetList(datasetOrigin=None):
    '''Parse input arguments to create a list of files.'''
    if datasetOrigin is None:
        print('WARNING: No DSID or dataset file specified.')
        sys.exit(1)
    if os.path.exists(datasetOrigin):
        with open(datasetOrigin, 'r') as dsidFile:
            fullList = dsidFile.read().splitlines()
    else:
        fullList = datasetOrigin.split(',')

    partialList = []
    for dsid in fullList:
        # remove any whitespace from the file name
        dsid = dsid.strip()
        # skip commented or blank lines
        if dsid.find('#')==0 or dsid=='': continue
        partialList.append(dsid)

    return partialList

def plotVariable(options,inputList):
    '''Plots a given variable from the list of files.'''
    plotOptions = options.plot
    var,nBin,binLo,binHi = plotOptions.split(',')
    nBin,binLo,binHi = int(nBin),float(binLo),float(binHi)
    histogram = ROOT.TH1F('h_'+var,var,nBin,binLo,binHi)

    for fileName in inputList:
        f = ROOT.TFile.Open(fileName)
        t = ROOT.xAOD.MakeTransientTree(f,'CollectionTree')
        nEntries = t.GetEntries() if not options.debug else nDebug
        for event in range(nEntries):
            t.GetEvent(event)
            for particle in getattr(t,options.particleContainer):
                value = particle.auxdata(options.vartype)(var)
                histogram.Fill(value)
    c = ROOT.TCanvas()
    c.SetLogy()
    histogram.Draw()
    c.SaveAs(options.output+'/canvas.png')
    c.SaveAs(options.output+'/canvas.eps')
    # c.SaveAs(options.output+'/canvas.C')

def plotCount(options,inputList):
    '''Plots length of a vector branch.'''
    plotOptions = options.plot
    particle = options.particleContainer
    nBin,binLo,binHi = plotOptions.split(',')
    nBin,binLo,binHi = int(nBin),float(binLo),float(binHi)
    histogram = ROOT.TH1F('h_'+particle,'',nBin,binLo,binHi)

    for fileName in inputList:
        f = ROOT.TFile.Open(fileName)
        t = ROOT.xAOD.MakeTransientTree(f,'CollectionTree')
        nEntries = t.GetEntries() if not options.debug else nDebug
        for event in range(nEntries):
            t.GetEvent(event)
            histogram.Fill(len(getattr(t,options.particleContainer)))
    c = ROOT.TCanvas()
    histogram.Draw()
    c.SaveAs(options.output+'/canvas.png')
    c.SaveAs(options.output+'/canvas.eps')

def eventCount(options,inputList):
    t = ROOT.TChain('CollectionTree')
    for fileName in inputList:
        t.Add(fileName)
    print(t.GetEntries())

if __name__ == '__main__':
    # Parse command line options
    details = """Examples:
python AODAnalysis.py --plot Jpsi_mass,20,2000,3600 input_files.txt
"""
    parser = optparse.OptionParser(usage='Usage: %prog [options]',epilog=details)
    parser.add_option('--output', default='/data_ceph/cburton/',
        help='Directory for output files.')
    parser.add_option('--parallel',action='store_true',
        help='Activate parallelization.')
    parser.add_option('--debug',action='store_true',
        help='Skips lines sending commands.')
    parser.add_option('--particleContainer',default='BPHY9OniaCandidates',
        help='Choose which particle container to loop over.')
    parser.add_option('--vartype',default='float',
        help="Variable type of the thing you're trying to plot.")
    parser.add_option('--plot',default='',
        help='Plot variable specified in argument.')
    parser.add_option('--count',action='store_true',
        help='Count total number of events in file list.')
    options, args = parser.parse_args()

    # Gather the inputs
    inputList = getDatasetList(args[0])

    # setup ROOT xAOD access for python
    ROOT.xAOD.Init().isSuccess()

    if options.plot!='' and options.count:
        plotCount(options,inputList) 
    elif options.plot != '':
        plotVariable(options,inputList)
    elif options.count:
        eventCount(options,inputList)
    else:
        print('Unknown operation.')

    ROOT.xAOD.ClearTransientTrees()

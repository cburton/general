import uproot
import pandas as pd
import argparse
from time import time
from math import floor

def retrieveDF(inFileName, branchKeys, treeName):
    '''Open file in uproot, and return pandas dataframe of selected tree.'''
    inputFile = uproot.open(inputFileName)
    inputTree = inputFile[treeName]
    return inputTree.pandas.df(branchKeys)

def printStatus(iteration,total,startTime):
    '''Print status of the loop over input files.'''
    deltaT = time()-startTime
    deltaTStr = str(int(floor(deltaT/60)))+'m, '+str(int(round(deltaT) % 60))+'s.'
    print('Converting file {0}/{1}. Time elapsed: {2}'\
          .format(iteration+1,total,deltaTStr))

def parse():
    '''Parse command line arguments.'''
    parser = argparse.ArgumentParser()
    parser.add_argument('input',
        help='Input files. Skips any starting with "#"')
    parser.add_argument('output',
        help='Ouput HDF file.')
    parser.add_argument('-t','--tree',default='nominal',
        help='Specify the name of the input tree.')
    parser.add_argument('-b','--branches',default=None,
        help='Specify a list of branches.')
    parser.add_argument('-f','--fixed',action='store_true',
        help='Append to output data file.')
    parser.add_argument('-q','--quiet',action='store_true',
        help='Supress print statements.')
    return parser.parse_args()

if __name__=='__main__':
    parsed = parse()

    # Read input file
    with open(parsed.input) as inputFile:
        inputFileNames = inputFile.read().replace(' ',',').replace('\n',',').split(',')
    # Remove any commented-out files
    inputFileNames = [_ for _ in inputFileNames if len(_)>0 and (_.strip())[0]!='#']

    # Load only specific branches, if desired.
    branchKeys = parsed.branches.split(',') if parsed.branches else ['*']

    # Static output if '--fixed' argument is used.
    outputFormat = 'fixed' if parsed.fixed else 'table'

    start,nInputs = time(),len(inputFileNames) # For status printing
    df = pd.DataFrame() # Append to empty dataframe

    # Loop over input files, creating a DF, and appending it to the master
    for i,inputFileName in enumerate(inputFileNames):
        if not parsed.quiet: printStatus(i,nInputs,start)

        thisDF = retrieveDF(inputFileName,branchKeys,parsed.tree)
        df = df.append(thisDF)

    # Save output to hdf. kwargs are to make selections when reading back from HDF
    df.to_hdf(parsed.output,parsed.tree,format=outputFormat,data_columns=True)
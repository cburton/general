#!/usr/bin/env python

__author__ = 'C. D. Burton'
__doc__ = '''Checks rucio for an existing NTUP_PILEUP file for a list of DSIDs.

See source for detailed comments.

Example usage:
    lsetup rucio
    python generatePRW.py --inputDAODFile mc16.BPHY9.2021-05.txt
'''

import os,sys,argparse,subprocess
try:
    import rucio.client
except:
    print 'Could not load pyAMI and/or rucio module.'
    sys.exit(1)

rucioclient = rucio.client.Client()
scope = 'mc16_13TeV'

def listDatasets(theScope, datasetPattern, containersOnly=False):
    """Lists containers (and possibly datasets) matching some pattern."""
    response = rucioclient.list_dids(scope = theScope, filters = {'name' : datasetPattern})
    if containersOnly:
        return [l.encode('ascii') for l in response if l.find('_tid')<0]
    else:
        return [l.encode('ascii') for l in response]

def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('--inputDAODFile',action='store',help='Text file containing list of DAOD datasets',required=True)
    parser.add_argument('--download',action='store_true',help='Download NTUP_PILEUP containers after search.')
    parser.add_argument('--dir',default='.',help='Location to place the downloaded files.')
    parser.add_argument('--debug',action='store_true',help='Debug mode will not download.')
    args = parser.parse_args()

    # read datasets into list
    with open(args.inputDAODFile) as f:
        datasets = f.read().splitlines()
    
    ntupDatasets=[]
    for dataset in datasets:
        # skip blank lines
        if dataset=='':
            continue

        # skip commented lines
        if dataset.startswith('#'):
            continue

        # strip the scope if present
        dataset = dataset.rsplit(':')[-1].strip()
        if len(dataset)==0:
            continue

        # strip a triling directory '/'
        dataset = dataset.strip('/')

        print 'Checking %s.' % dataset

        # get the form of the NTUP_PILEUP dataset name
        ntup_pileup = dataset.split('.')

        # replace derivation name with NTUP_PILEUP
        ntup_pileup[4] = 'NTUP_PILEUP'

        ntup_pileup[5] = ntup_pileup[5].split('_')
        ntup_pileup[5] = [_ for _ in ntup_pileup[5] if _[0]!='p']
        ntup_pileup[5] = '_'.join(ntup_pileup[5]) + '_p*'
        ntup_pileup = '.'.join(ntup_pileup)

        ntupDatasetList = listDatasets(scope,ntup_pileup,True)
        badDatasets = 0
        for ntupDataset in ntupDatasetList:
            ntupFileNamesRucio = rucioclient.list_files(scope,ntupDataset)
            totalBytes = 0
            for ntupFileName in ntupFileNamesRucio:
                totalBytes += ntupFileName.get('bytes')

            if totalBytes>0:
                ntupDatasets.append(ntupDataset)
                break
            else:
                badDatasets += 1
        if badDatasets==len(ntupDatasets):
            print 'Could not find PRW file for %s' % dataset

    for ntupDataset in ntupDatasets:
        print ntupDataset

    if args.download:
        # create output path
        path = args.dir+'/'
        cmd = ['rucio','get']
        cmd.extend(['--dir',path])
        for ntupDataset in ntupDatasets:
            cmd.append(ntupDataset)
        # make the call
        if not args.debug:
            subprocess.call(cmd)
        else:
            print cmd

if __name__ == "__main__":
    main()
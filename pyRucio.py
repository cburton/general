#!/usr/bin/env python
"""Run common rucio operations from the command line.

Name: pyRucio.py
Author: C. D. Burton
Email: burton@utexas.edu
Date: 7 September 2018

Note: Requires python3. Run e.g. "asetup Athena,master,latest".

command line options:
    debug: Skips the downloading step if this flag is raised
    dir: The output directory for downloaded files or subdirectories
    nFiles: Number of files desired. If absent, is set to 'All'.
    datasetFile: File with list of datasets
    dataset: One line of datasetFile (file not used)
"""
import os, sys, errno
import subprocess
import argparse
from glob import glob
import rucio.client
import rucio.common

def mkdir_p(path):
    '''A python function that mimics 'mkdir -p' Unix command.'''
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(path): pass
        else: raise

def listDatasets(theScope, datasetPattern, containersOnly=False):
    '''Lists containers (and possibly datasets) matching some pattern.'''
    response = rc.list_dids(scope=theScope, filters={'name':datasetPattern})
    if containersOnly:
        return [l.encode('ascii') for l in response if l.find('_tid')<0]
    else:
        return [l.encode('ascii') for l in response]

def getDatasetList(datasetOrigin=None):
    '''Parse input arguments to create a list of files.'''
    if datasetOrigin is None:
        print('WARNING: No DSID or dataset file specified.')
        sys.exit(1)
    if os.path.exists(datasetOrigin):
        with open(datasetOrigin, 'r') as dsidFile:
            fullList = dsidFile.read().splitlines()
    else:
        fullList = datasetOrigin.split(',')

    partialList = []
    for dsid in fullList:
        # remove any whitespace from the file name
        dsid = dsid.strip()
        # skip commented or blank lines
        if dsid.find('#')==0 or dsid=='': continue
        partialList.append(dsid)

    return partialList

def openLifetimeFile(parsed):
    ''''''
    if parsed.lifetime is not None:
        with open(parsed.lifetime, 'r') as lifetimeFile:
            expiredList = lifetimeFile.read().splitlines()
            expiredList = [_.split(' ')[0] for _ in expiredList]
        return expiredList
    else:
        print('No lifetime file specified.')
        sys.exit(1)

def listFiles(parsed):
    ''''''
    datasetList = getDatasetList(parsed.datasets)
    for dataset in datasetList:
        # find the scope of the dataset
        if dataset.find(':')>0:
            scope = dataset.split(':')[0]
            dataset = dataset[dataset.find(':')+1:]
        else:
            if dataset[:4]=='user':
                scope = dataset.split('.')[:2]
                scope = '.'.join(scope)
                dsid = dataset.split('.')[2]
            else:
                scope = dataset.split('.')[0]
                dsid = dataset.split('.')[1]
        dsid = dataset.split('.')[2] if dataset[:4]=='user' else dataset.split('.')[1]
        if '_r9364' in dataset:
            profile = 'mc16a'
        elif '_r10201' in dataset:
            profile = 'mc16d'
        elif '_r10724' in dataset:
            profile = 'mc16e'
        names = [_['name'] for _ in rc.list_files(scope, dataset)]
        print(f'\t\t<dataset dsid="{dsid}">')
        print(f'\t\t\t<profile name="{profile}" tag="{profile[-1]}"><!-- {len(names)} -->')
        [print(f'\t\t\t\t<file>{scope}:{_}</file>') for _ in names]
        print('\t\t\t</profile>')
        print('\t\t</dataset>')

def listReplicas(parsed):
    '''Similar to list-file-replicas (without weird table stuff)'''
    datasetList = getDatasetList(parsed.datasets)
    for dataset in datasetList:

        # find the scope of the dataset
        if dataset.find(':')>0:
            scope = dataset.split(':')[0]
        else:
            if dataset.find('user.')==0:
                scope = 'user.' + dataset.split('.')[1]
            else:
                scope = dataset.split('.')[0]

        # find the profile of the dataset
        if '_r9364' in dataset:
            profile = 'mc16a'
        elif '_r10201' in dataset:
            profile = 'mc16d'
        elif '_r10724' in dataset:
            profile = 'mc16e'

        did = {'scope': scope, 'name': dataset}
        replicas = rc.list_replicas([did], rse_expression=parsed.rse, schemes=[parsed.protocol])
        pfns = []
        for replica in replicas:
            i = 0
            if 'bytes' in replica:
                for rse in replica['rses']:
                    for pfn in replica['rses'][rse]:
                        if parsed.n is None or i < parsed.n:
                            pfns.append(pfn)
                            i += 1

        print(f'\t\t<dataset dsid="{dataset[len(scope):].split(".")[1]}">')
        print(f'\t\t\t<profile name="{profile}" tag="{profile[-1]}"><!-- {len(pfns)} -->')
        [print(f'\t\t\t\t<file>{_}</file>') for _ in pfns]
        print('\t\t\t</profile>')
        print('\t\t</dataset>')

def checkMC(parsed):
    '''Finds requested datasets and checks for available files.'''
    datasetList = getDatasetList(parsed.datasets)
    expiredList = None

    # Loop over datasets that we would like to process
    emptyDatasets = []
    expiredDatasets = []
    printDataset = max(int(len(datasetList) / 10), 1)
    for i, dataset in enumerate(datasetList):
        if not i%printDataset:
            print(f'Checking dataset {i} of {len(datasetList)}.')
        # find the scope of the dataset
        if dataset.find(':')>0:
            scope = dataset.split(':')[0]
        else:
            scope = dataset.split('.')[0]
        # check that the dataset exists, and that there are files available to download
        try:
            fileList = rc.list_files(scope, dataset)
        except rucio.common.exception.DataIdentifierNotFound:
            print(f'WARNING: Could not find dataset {dataset}.')
            emptyDatasets.append(dataset)
            continue
        sizeList = [file.get('bytes') for file in fileList]
        totalSize = sum(sizeList)
        totalFiles = len(sizeList)
        if totalSize==0:
            print(f'WARNING: No files available in dataset {dataset}.')
            emptyDatasets.append(dataset)
            continue

        if parsed.lifetime is not None:
            if expiredList is None:
                expiredList = openLifetimeFile(parsed)
            response = rc.list_content(scope, dataset)
            for constituent in response:
                if constituent['name'] in expiredList:
                    print(f'WARNING: Dataset {dataset} is slated for deletion.')
                    expiredDatasets.append(constituent['name'])

    if len(emptyDatasets)>0:
        print('The following datasets have no available files:')
        [print(_) for _ in emptyDatasets]
    else:
        print('All datasets have available files.')

    if len(expiredDatasets)>0:
        print('The following datasets are slated for deletion:')
        [print(_) for _ in expiredDatasets]
    else:
        print('All datasets are safe from expiration.')

def getMC(parsed):
    '''Finds and downloads requested datasets.'''
    datasetList = getDatasetList(parsed.datasets)

    # Loop over datasets that we would like to process
    failedDatasets = []
    for dataset in datasetList:
        # find the scope of the dataset
        if dataset.find(':')>0:
            scope = dataset.split(':')[0]
        else:
            scope = dataset.split('.')[0]
        # check that the dataset exists, and that there are files available to download
        try:
            fileList = rc.list_files(scope, dataset)
        except rucio.common.exception.DataIdentifierNotFound:
            print(f'WARNING: Could not find dataset {dataset}.')
            failedDatasets.append(dataset)
            continue
        sizeList = [file.get('bytes') for file in fileList]
        totalSize = sum(sizeList)
        totalFiles = len(sizeList)
        if totalSize==0:
            print(f'WARNING: No files available in dataset {dataset}.')
            failedDatasets.append(dataset)
            continue

        # if downloading, calculate the number of files to get.
        nExistingFiles = len(glob(parsed.dir + '/' + dataset + '/*.root*'))
        if parsed.nFiles=='all' and nExistingFiles!=totalFiles:
            nToDownload = totalFiles - nExistingFiles
            nrandom = ''
        elif nExistingFiles < int(parsed.nFiles):
            nToDownload = int(parsed.nFiles) - nExistingFiles
            nrandom = ['--nrandom', str(nToDownload)]
        else:
            print(f'Dataset {dataset} already complete. Skipping.')
            continue

        # run the commands
        cmd = ['rucio', 'get']
        cmd.extend(['--dir', parsed.dir])
        cmd.extend(nrandom)
        cmd.append(dataset)
        print(f'Downloading {nToDownload} files from {dataset}.')
        if parsed.debug:
            print(cmd)
        else:
            mkdir_p(parsed.dir)
            subprocess.call(cmd)

        # change permissions of the downloaded files
        cmd = ['chmod', '-R', '755', parsed.dir + '/' + dataset]
        if parsed.debug:
            print(cmd)
        else:
            subprocess.call(cmd)

    if len(failedDatasets)>0:
        print('Download failed for the following datasets:')
        [print(_) for _ in failedDatasets]

if __name__ == '__main__':
    rc = rucio.client.Client()

    parser = argparse.ArgumentParser(
        description="Performs various Rucio operations using the program's Python API.",
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog='Requires python3. First run e.g. "asetup Athena,master,latest".\n'+\
               'Example usages:\n'+\
               '    ./pyRucio.py getMC user.cburton.my_dataset\n'+\
               '    ./pyRucio.py getMC my_file.txt --nFiles 2\n'+\
               '    ./pyRucio.py getMC my_file.txt --dir /data_ceph/cburton/ntuples/\n'+\
               '    ./pyRucio.py checkMC user.cburton.my_dataset\n'+\
               '    ./pyRucio.py checkMC my_file.txt --lifetime my_lifetime.txt\n'\
               '    ./pyRucio.py listFiles user.cburton.my_dataset\n'+\
               '    ./pyRucio.py listFiles my_file.txt\n'+\
               '    ./pyRucio.py listReplicas user.cburton.my_dataset\n'+\
               '    ./pyRucio.py listReplicas my_file.txt\n',
    )
    parser.add_argument('--debug', action='store_true', help='Commands will be printed, not executed.')
    subparsers = parser.add_subparsers()

    # get MC command
    parser_getMC = subparsers.add_parser('getMC', help='Download MC files.')
    parser_getMC.add_argument('--dir', default='/data_ceph/cburton/', type=str)
    parser_getMC.add_argument('--nFiles', default='all', type=str)
    parser_getMC.add_argument('datasets', default=None, type=str)
    parser_getMC.set_defaults(function=getMC)

    # check MC command
    parser_checkMC = subparsers.add_parser('checkMC', help='Check existance and lifetime of MC files.')
    parser_checkMC.add_argument('--lifetime', default=None, type=str)
    parser_checkMC.add_argument('datasets', default=None, type=str)
    parser_checkMC.set_defaults(function=checkMC)

    # list files command
    parser_listFiles = subparsers.add_parser('listFiles', help='List files of MC datasets.')
    parser_listFiles.add_argument('datasets', default=None, type=str)
    parser_listFiles.set_defaults(function=listFiles)

    # list replicas command
    parser_listReplicas = subparsers.add_parser('listReplicas', help='List xrootd addresses of a dataset.')
    parser_listReplicas.add_argument('datasets', default=None, type=str)
    parser_listReplicas.add_argument('--protocol', default='root', type=str)
    parser_listReplicas.add_argument('--rse', default=None, type=str)
    parser_listReplicas.add_argument('--n', default=None, type=int)
    parser_listReplicas.set_defaults(function=listReplicas)

    # create parsed object
    parsed = parser.parse_args()

    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)

    parsed.function(parsed)

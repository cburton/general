#!/usr/bin/env python
"""
Name: DumpPlots.py
Purpose: Dump histograms from a root file into image files. Script opens the 
         file, searches for histogram objects recursively in the TDirectory 
         structure, and creates a unix-directory containing the image files 
         with the same structure.
Author: C.D. Burton
Email: burton@utexas.edu
Date: 10 May 2018
"""

import ROOT, os, sys, optparse, errno
ROOT.gROOT.SetBatch(ROOT.kTRUE)

def mkdir_p(path): # equivalent to "mkdir -p" in command line
    try: os.makedirs(path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(path): pass
        else: raise

def savePath(directory): 
    # directory path comes as file.root:/location/to/histogram
    # strips off the filename and colon to return pure directory
    rootDirPath = directory.GetPath()
    colon = rootDirPath.find(':')
    if colon>=0: 
        rootDirPath = rootDirPath[colon+1:]
    return rootDirPath

def saveHistogram(directory, histogram, options):
    # draws histogram with options and saves canvas as pdf
    c = ROOT.TCanvas('c','c',600,600)
    outdir = options.outdir
    histName = histogram.GetName()
    rootDirPath = savePath(directory)
    ymax = histogram.GetBinContent(histogram.GetMaximumBin())
    histogram.GetYaxis().SetRangeUser(0,ymax*1.1)
    histogram.Draw(options.draw)
    imageTypeList = (options.imageType).split(',')
    for imageType in imageTypeList:
        c.SaveAs(outdir+'/'+rootDirPath+'/'+histName+'.'+imageType)

def gatherHistograms(directory, options):
    # output structure matches directory structure of root file
    rootDirPath = savePath(directory)
    for key in directory.GetListOfKeys(): 
        # looping over TDirectory, get the list of objects to be drawn.
        # get name and root class of given object
        keyName = key.GetName()
        rootClass = ROOT.TClass.GetClass(key.GetClassName())

        # For each object, check whether the class is allowed to be plotted.
        # 1) If a specific class was specified in command line options, allow only that.
        if options.rootClass:
            allowedClass = rootClass.InheritsFrom(options.rootClass)
        # 2) Otherwise, look for generic histogram objects.
        else:
            oneD = ['TH1','TGraph','TEfficiency','TProfile']
            twoD = ['TH2']
            isOneD = any([rootClass.InheritsFrom(allowed1D) for allowed1D in oneD])
            isTwoD = any([rootClass.InheritsFrom(allowed2D) for allowed2D in twoD])
            allowedClass = isOneD or isTwoD

        if allowedClass: # save histogram if allowed
            print '====> Saving histogram',keyName
            histogram = directory.Get(keyName)
            saveHistogram(directory,histogram,options)
        elif rootClass.InheritsFrom('TDirectory'): # recurse to sub-TDirectory
            print '====> Entering directory',keyName
            mkdir_p(options.outdir+'/'+rootDirPath+'/'+keyName)
            gatherHistograms(key.ReadObj(), options)

def processFile(inFileName, options):
    # open input file
    print '==> Opening input file.'
    inFile = ROOT.TFile.Open(inFileName, 'READ')
    if not inFile.IsOpen():
        print 'ERROR: cannot open %s' % inFileName
        return False
    
    # Search through root file recursively and draw histograms.
    print '==> Gathering available histograms.'
    gatherHistograms(inFile, options)

    print '==> Histogram images saved to', options.outdir
    return True

if __name__=="__main__":
    # parse command line options
    parser = optparse.OptionParser(usage='Usage: %prog [options] inputfile')
    parser.add_option('--outdir', default='./histograms',
                      help='Directory for web output')
    parser.add_option('--normalize', default=False, action='store_true',
                      help='Normalize reference histograms for display')
    parser.add_option('--draw', default='',
                      help='Set options for "Draw()" call.')
    parser.add_option('--rootClass', default='',
                      help='Choose a specific root class for which plots should be made.')
    parser.add_option('--imageType', default='png',
                      help='Choose the file extension. Default: png')
    options, args = parser.parse_args()
    
    # one argument: file name
    if len(args)!=1:
        parser.print_help()
        sys.exit(1)
    fname = args[0]

    if options.imageType.find('.')>=0:
        print 'Cannot have "." in image type.'
        sys.exit(1)

    # create the output directory if necessary
    mkdir_p(options.outdir)

    # begin analysis on file
    result = processFile(fname,options)
    if result == True:
        sys.exit(0)
    else:
        sys.exit(1)
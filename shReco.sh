#! /bin/bash
# -----------------------------------------------------------------------
# AOD->DAOD conversion for small samples locally. Parallelized for speed. 
# Of course, if using many files, use the grid instead.
#
# Name: shReco.sh
# Author: Charles D. Burton
# Email: burton@utexas.edu
# Date: 27 June 2018
# -----------------------------------------------------------------------
if [ -z "$1" ]
then 
    echo "Input file not specified. Usage: shReco.sh [inputFileName]"
    exit 1
fi

run_job ()
{
    local inputAOD=$1
    local dir=$(echo "$1" | cut -d "/" -f 5)
    mkdir $dir
    cd $dir
    echo "Reco_tf.py --inputAODFile $inputAOD --outputDAODFile root --reductionConf BPHY9"  
    Reco_tf.py --inputAODFile $inputAOD --outputDAODFile root --reductionConf BPHY9
    # echo "mv DAOD_BPHY9.root ../DAOD_BPHY9.$dir.root"
    mv DAOD_BPHY9.root ../DAOD_BPHY9.$dir.root
    # echo "mv log.AODtoDAOD ../$dir.log"
    mv log.AODtoDAOD ../$dir.log
    cd ..
    rm -r $dir
}

while read line
do
    # Skip lines that begin with #, as well as blank lines
    case "$line" in \#*) continue ;; "") continue ;; esac
    # Run the above function in parallel for each
    run_job "$line" &
done < "$1"

#!/bin/python
import xml.etree.ElementTree as ET
import numpy as np
import sys,optparse

def getHeaderAndInit(fileNameList):
    header,information = None,None

    tree = ET.parse(fileNameList[0])
    root = tree.getroot()

    for child in root:
        if child.tag=='header':
            header = child

        if child.tag=='init':
            lines = child.text.strip().split('\n')
            informationList = lines[0].split()
            informationList[-1] = str(len(fileNameList))
            information = '  '.join(informationList)

    if header==None or information==None:
        print 'FATAL: Could not retrieve header or process information. Exiting.'
        sys.exit(1)

    return header,information

def getXsec(root):
    for child in root:
        if child.tag=='init':
            lines = child.text.strip().split('\n')
            procInfo = lines[1].split()
            xsec = procInfo[0].strip()
            return float(xsec)

def getProcessInfo(root):
    for child in root:
        if child.tag=='init':
            lines = child.text.strip().split('\n')
            procInfoList = lines[1].split()
            return '  '.join(procInfoList)

def makeInit(generalInfo,processInfo):
    allInfoList = [generalInfo]+processInfo.values()
    init = ET.Element('init')
    init.text = '\n'+'\n'.join(allInfoList)+'\n'
    return init

def fixEvent(event):
    oldText = event.text
    newText = oldText.replace(' 9900443   ','     443   ')
    event.text = newText

if __name__=='__main__':
    parser = optparse.OptionParser(usage='Usage: %prog [options]')
    parser.add_option('-n','--nEvents',default='5000',metavar='N',
                      help='For testing without a board.')
    options, args = parser.parse_args()
    nEvents = int(options.nEvents)

    if len(args)==0:
        fileNameList = ['./sampledbaruetac1W+.lhe','./sampledbarupsi8W+.lhe']
    else:
        fileNameList = args

    # Once-only info
    header,information = getHeaderAndInit(fileNameList)

    xsecs = {}
    counts = {}
    events = {}
    infos = {}

    # First, open files and parse the LHE for events
    for fileName in fileNameList:
        tree = ET.parse(fileName)
        root = tree.getroot()

        # store xsec as float, zero the counters, and store the events as XML Elements
        processName = fileName.split('/')[-1].split('.')[0]
        xsecs[processName] = getXsec(root)
        counts[processName] = 0
        events[processName] = list(root.iterfind('event'))
        infos[processName] = getProcessInfo(root)

    totalXsec = sum(xsecs.values())
    xsecsNorm = {k: v/float(totalXsec) for k,v in xsecs.items()}

    # Begin writing the merged output file
    root = ET.Element('LesHouchesEvents version="1.0"')
    root.text = '\n'
    root.append(ET.Comment('\nFile generated with HELAC-ONIA and Chuck\'s merge script.\n'))
    root.append(header)
    root.append(makeInit(information,infos))
    outTree = ET.ElementTree(root)

    for i in range(nEvents):
        # Randomly sample the files according to the cross section
        sampleChoice = np.random.choice(list(xsecs.keys()),p=list(xsecsNorm.values()))

        # Get the next unused event
        try:
            event = events[sampleChoice][counts[sampleChoice]]
            fixEvent(event)
        except IndexError:
            print 'FATAL: Not enough events in input LHE file',sampleChoice
            sys.exit(1)

        # Add it to the output file and increment the appropriate counter
        counts[sampleChoice] += 1
        root.append(event)

    outTree.write('merged.lhe')
